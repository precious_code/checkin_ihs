<?php
class VisitsModel extends CI_Model{
	public function __construct(){
		parent::__construct();
		 $this->load->database();
	 }
    
     
     public function process($data2){

        $this->db->insert('visits',$data2);
     }

	 function get_all_visits(){
		$result = $this->db->order_by('createdat', 'DESC')->get('visits')->result();
		return $result;
	}

}