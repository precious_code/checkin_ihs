<?php
class EmployeeModel extends CI_Model{

	public function __construct(){
		parent::__construct();
		 $this->load->database();
	 }
	function get_all_employee(){
		$result=$this->db->get('employee');
		return $result;
	}

	function search_employee($title){
		$this->db->like('emp_fullname', $title , 'both');
		$this->db->order_by('emp_fullname', 'ASC');
		$this->db->limit(10);
		return $this->db->get('employee')->result();
	}

	function get_id($title){
		return array_shift($this->db->get_where('employee', array('emp_fullname'=>$title),1)->result());
	}

	function get_by_id($id){
		return array_shift($this->db->get_where('employee', array('employee_id'=>$id), 1)->result());
	}

	

}