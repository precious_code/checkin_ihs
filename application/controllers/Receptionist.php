<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Receptionist extends CI_Controller {

    function __construct(){
		parent::__construct();
		//$this->load->library('session');
		$this->load->model('ReceptionistModel');
		$this->load->model('VisitsModel');
		$this->load->model('EmployeeModel');
		error_reporting(0);
	}
    public function index()
    {
    
        $this->load->view('receptionist_login_view');
	}
	function login()
	{
		$found_user = $this->ReceptionistModel->authenticate_user($this->input->post('user_email'),$this->input->post('user_password'));
		if($found_user)
		{
			$this->session->set_userdata('loggedInUserID', $found_user->id);
			redirect('receptionist/dashboard');
		}
		else
		{
			$data['data']='invalid email or password';
			$this->load->view('receptionist_login_view',$data);
		}
	}

	function logout()
	{
		$this->session->unset_userdata('loggedInUserID');
		$this->load->view('receptionist_login_view');
	}
	function dashboard()
	{
		if(!$this->session->has_userdata('loggedInUserID')){redirect('receptionist/login');}
		$all  = $this->VisitsModel->get_all_visits();
		for ($i = 0; $i < sizeof($all); $i++) {
			$data['data'][$i]->employee 			= $this->EmployeeModel->get_by_id($all[$i]->employee_id)->emp_fullname;
			$data['data'][$i]->guestPurposeType 	= ($all[$i]->guest_purpose=='Official')? 'Official' : 'Unofficial';
			$data['data'][$i]->guestFullName 		= $all[$i]->guest_lastname .' '.$all[$i]->guest_firstname ;
			$data['data'][$i]->createdat 			= $all[$i]->createdat;
			$data['data'][$i]->guestPurpose 		= $all[$i]->guestPurpose;
			$data['data'][$i]->guestCompany 		= ($all[$i]->guest_company!='')? $all[$i]->guest_company : '<div class="red-text">None Provided.</div>';
			$data['data'][$i]->guestTelephone 		= $all[$i]->guest_telephone;
			$data['data'][$i]->guestEmail	 		= $all[$i]->guest_email;
			$data['data'][$i]->guestComment 		= $all[$i]->guest_comment;
			$data['data'][$i]->id 					= $all[$i]->id;
		}
		$this->load->view('receptionist_dashboard_view',$data);
	}
	
	function printtag(){
		$this->load->view('printslip_view');
	}

}

?>