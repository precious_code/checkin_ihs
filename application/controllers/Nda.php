<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Nda extends CI_Controller {

public function index()
{	
    date_default_timezone_set('Africa/Lagos');
    $filename = APPPATH .'third_party/test1.pdf';
define('FPDF_FONTPATH',APPPATH .'third_party/fpdf/font/');
require(APPPATH .'third_party/fpdf/fpdf.php');
$name = "Uchendu Precious . A";
$time = date(' H:i:a');
$day = date("jS ");
		$month =  date("F");
		$year = date("Y");
$html = "We hope you enjoy your visit, While you're here, you may see or hear information that is confidential to MTN, its members or business partners.
You agree not to photograph or record any information that you come across during your visit. If you or your company has a nondisclosure agreement (NDA) with MTN, by clicking the check box below you acknowledge that information you see or hear during your visit is confidential under that NDA, and we agree that the terms of the NDA will prevail over this agreement.
For those of you whose visit is not covered under an existing NDA, the information that you see or hear during your visit is the confidential information of MTN and you agree that you will not take,use, discuss, share or disclose this information without MTN's writeen permission. We agree that you don't need our permission if you can show that something is already generally publicly known through no fault of yours or breach by others.

Thanks you your cooperation. Have a great day.
Signed by :".$name."
Today :".$day.'-'.$month.'-'.$year."
Time:".$time;
$pdf = new FPDF('p','mm','A4');
$pdf -> AddPage();

$pdf -> setDisplayMode ('fullpage');

$pdf -> setFont ('times','B',20);
$pdf -> cell(200,30,"MTN NDA",0,1);

$pdf -> setFont ('times','B','20');
$pdf -> write (10,$html);

$pdf -> output ($filename,'F');  	
}
}

?>