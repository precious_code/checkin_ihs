<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin extends CI_Controller {

    function __construct(){
		parent::__construct();
		$this->load->model('EmployeeModel');
		$this->load->model('VisitsModel');
		error_reporting(0);
	}
    public function index()
    {
        $this->load->view('checkin_view');
	}
	function search(){

		$this->session->set_userdata('sender_firstname', $this->input->get('sender_firstname'));
				$this->session->set_userdata('sender_lastname', $this->input->get('sender_lastname'));
		$this->session->set_userdata('sender_radio', $this->input->get('sender_radio'));
		$this->session->set_userdata('sender_company', $this->input->get('sender_company'));
		$this->session->set_userdata('sender_purpose', $this->input->get('sender_purpose'));
		$this->session->set_userdata('sender_phone', $this->input->get('sender_phone'));
		$this->session->set_userdata('employee', $this->input->get('title'));
		$this->session->set_userdata('sender_email', urldecode($this->input->get('sender_email')));
		
		$title=urldecode($this->input->get('title'));
		$data['data']=$this->EmployeeModel->search_employee($title);
		

		//
		$this->load->view('printslip_view', $data);
		 $this->send_mail();
		
	
	}
	public function send_mail(){
    
			$filename= APPPATH.'third_party/'.$this->session->userdata('sender_company').'.pdf';
    date_default_timezone_set('Africa/Lagos');
     $config = Array(
    'protocol' => 'mail',
    'smtp_host' => 'ssl://smtp.googlemail.com',
    'smtp_port' => 465,
    'smtp_user' => 'checkin@meytrics.com', // change it to yours
    'smtp_pass' => 'techbarn2018', // change it to yours
    'mailtype' => 'html',

    'smtp_crypto' => 'security',
    'wordwrap' => TRUE
  ); 
$name = $this->session->userdata('sender_lastname').' '.$this->session->userdata('sender_firstname');
$time = date(' H:i:a');
$day = date("jS ");
		$month =  date("F");
		$year = date("Y");
$html = "We hope you enjoy your visit, While you're here, you may see or hear information that is confidential to IHS, its members or business partners.
You agree not to photograph or record any information that you come across during your visit. If you or your company has a nondisclosure agreement (NDA) with IHS, by clicking the check box below you acknowledge that information you see or hear during your visit is confidential under that NDA, and we agree that the terms of the NDA will prevail over this agreement.
For those of you whose visit is not covered under an existing NDA, the information that you see or hear during your visit is the confidential information of IHS and you agree that you will not take,use, discuss, share or disclose this information without IHS's writeen permission. We agree that you don't need our permission if you can show that something is already generally publicly known through no fault of yours or breach by others.

Thanks for your cooperation. Have a great day.
Signed by :".$name."
Today :".$day.'-'.$month.'-'.$year."
Time:".$time;

		 
define('FPDF_FONTPATH',APPPATH .'third_party/fpdf/font/');
require(APPPATH .'third_party/fpdf/fpdf.php');

$pdf = new FPDF('p','mm','A4');
$pdf -> AddPage();

$pdf -> setDisplayMode ('fullpage');

$pdf -> setFont ('times','B',20);
$pdf -> cell(200,30,"IHS NDA",0,1);

$pdf -> setFont ('times','B','20');
$pdf -> write (10,$html);
   $pdf -> output ($filename,'F');
   	$subject = "Your signed agreement from ". $this->session->userdata('sender_company');
   $this->load->library('email', $config);
       $this->email->set_newline("\r\n");
       $this->email->set_mailtype("html");  
       $this->email->from('no-reply@techbarn.ng'); // change it to yours
       $this->email->to($this->session->userdata('sender_email'));// change it to yours
	   $this->email->subject('Your signed agreement from ');
	   $mail_data['firstname'] = $this->session->userdata('sender_firstname');
	   $mail_data['company'] = $this->session->userdata('sender_company');
 	//    $this->email->message($message);
		$body= $this->load->view('emails/nda_view', $mail_data, true);
          $this->email->message($body);
       $this->email->attach($filename);
       
    if($this->email->send())
     {
       //echo 'Email sent.' ;
       echo '<script type="text/javascript">';
	echo 'setTimeout(function () { swal("Done!","NDA Document has been sent.","success");';
	echo '},600);</script>';
	$this->insertVisitData();
    //  $this->load->view('printslip_view');
     }
     else
     {
      echo '<script type="text/javascript">';
      echo 'setTimeout(function () { swal("Oops!","A Fatal Error has Occurred","error");';
      echo '},600);</script>';
      show_error($this->email->print_debugger());
     }
		
		   
		}

    function get_autocomplete(){
		if (isset($_GET['term'])) {
		  	$result = $this->EmployeeModel->search_employee($_GET['term']);
		   	if (count($result) > 0) {
		    foreach ($result as $row)
		     	$arr_result[] = array(
					'label'	=> $row->emp_fullname,
				);
		     	echo json_encode($arr_result);
		   	}
		}
	
}
	public function insertVisitData(){

		
			$t = date('Y-m-d H:i:s');
		$employee_id = $this->session->userdata('employee_id');
		$guest_firstname = $this->session->userdata('sender_firstname');
		$guest_lastname = $this->session->userdata('sender_lastname');
		$guest_company = $this->session->userdata('sender_company');
		$guest_comment = $this->session->userdata('sender_purpose');
		$guest_purpose = $this->session->userdata('sender_radio');
		$guest_telephone = $this->session->userdata('sender_phone');
		$guest_email = $this->session->userdata('sender_email');
		$createdat = $time;

		$data2 = array(
			'employee_id' => $employee_id,
			'guest_firstname' =>$guest_firstname,
			'guest_lastname'=>$guest_lastname,
			'guest_company'=>$guest_company,
			'guest_comment'=>$guest_comment,
			'guest_purpose'=>$guest_purpose,
			'guest_telephone'=>$guest_telephone,
			'guest_email'=>$guest_email,
			'createdat' =>$createdat,
		);
		$this->VisitsModel->process($data2);
			

	}

}

?>