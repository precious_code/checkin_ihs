<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcheckin extends CI_Controller {

    function __construct(){
		parent::__construct();
		$this->load->model('EmployeeModel');
		$this->load->model('VisitsModel');
		error_reporting(0);
	}
    public function mindex()
    {
        $this->load->view('checkin_view_mobile');
	}
	function search(){
		$this->session->set_userdata('sender_firstname', $this->input->get('sender_firstname'));
		$this->session->set_userdata('sender_lastname', $this->input->get('sender_lastname'));
		$this->session->set_userdata('sender_radio', $this->input->get('sender_radio'));
		$this->session->set_userdata('sender_company', $this->input->get('sender_company'));
		$this->session->set_userdata('sender_purpose', $this->input->get('sender_purpose'));
		$this->session->set_userdata('sender_phone', $this->input->get('sender_phone'));
		 $this->session->set_userdata('employee', $this->input->get('title'));
		$this->session->set_userdata('sender_email', urldecode($this->input->get('sender_email')));
		
		$title=urldecode($this->input->get('title'));
		$data['data']=$this->EmployeeModel->search_employee($title);
		
		
		//
		$this->load->view('printslip_view', $data);
		
		$this->send_mail();
		
	
	}
	public function send_mail(){
    
		$config = Array(
			'protocol' => 'mail',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'checkin@meytrics.com', // change it to yours
			'smtp_pass' => 'techbarn2018', // change it to yours
			'mailtype' => 'html',
		
			'smtp_crypto' => 'security',
			'wordwrap' => TRUE
		  ); 
		 
		 $this->email->initialize($config);
			//$email_to = $this->input->post("email_id");
		//$this->email->to($email_to);
		
			 //  $message = '';
				$message = "
			 <html>
			   <head>
			   </head>
			   <body>
				 <p>Good Day Sir,</p>
				 <p>A Visitor with is here to see you with the following information:</p>".
				 "<p> Fullname:".$this->session->userdata('sender_fullname')."</p>".
				 "<p> Email to:".$this->session->userdata('email_val')."</p>".
				 "<p> Email from:".$this->session->userdata('sender_email')."</p>".
				 "<p> Phone Number:".$this->session->userdata('sender_phone')."</p>".
				 "<p> Purpose:".$this->session->userdata('sender_radio')."</p>".
				 "<p> Company where the visitor comes from:".$this->session->userdata('sender_company')."</p>".
				 "<p> Message Body:".$this->session->userdata('sender_purpose')."</p>";
			   
			   $this->email->set_newline("\r\n");
			   $this->email->from($this->session->userdata('sender_email'), $this->session->userdata('sender_fullname'));
				 $this->email->reply_to($this->session->userdata('email_val'));
				 $this->email->to($this->session->userdata('email_val'));
			   $this->email->subject('Visitor Notification');
			   $this->email->message($message);
			   
			  
			 if($this->email->send())
			 {
			   //echo 'Email sent.' ;
			   echo '<script type="text/javascript">';
			echo 'setTimeout(function () { swal("Done!","Email Sent!! Employee has been Notified.","success");';
			echo '},600);</script>';
			  
				
			 }
			 else
			 {
			  echo '<script type="text/javascript">';
			  echo 'setTimeout(function () { swal({ 
				title: "Error",
				 text: "wrong user or password",
				  type: "error" 
				},
				function(){';
				echo " window.location.href = '<?php echo site_url('checkin');?>";
			  echo '});';
			  echo '},600);</script>';
			  $this->insertVisitData();
			  show_error($this->email->print_debugger());
			 }
		
		
		   
		}

    function get_autocomplete(){
		if (isset($_GET['term'])) {
		  	$result = $this->EmployeeModel->search_employee($_GET['term']);
		   	if (count($result) > 0) {
		    foreach ($result as $row)
		     	$arr_result[] = array(
					'label'	=> $row->emp_fullname,
				);
		     	echo json_encode($arr_result);
		   	}
		}
	
		
		}
		public function insertVisitData(){

		
			$t = date('Y-m-d H:i:s');
		$employee_id = $this->session->userdata('employee_id');
		$guest_firstname = $this->session->userdata('sender_firstname');
		$guest_lastname = $this->session->userdata('sender_lastname');
		$guest_company = $this->session->userdata('sender_company');
		$guest_comment = $this->session->userdata('sender_purpose');
		$guest_purpose = $this->session->userdata('sender_radio');
		$guest_telephone = $this->session->userdata('sender_phone');
		$guest_email = $this->session->userdata('sender_email');
		$createdat = $t;

		$data2 = array(
			'employee_id' => $employee_id,
			'guest_firstname' =>$guest_firstname,
			'guest_lastname'=>$guest_lastname,
			'guest_company'=>$guest_company,
			'guest_comment'=>$guest_comment,
			'guest_purpose'=>$guest_purpose,
			'guest_telephone'=>$guest_telephone,
			'guest_email'=>$guest_email,
			'createdat' =>$createdat,
		);
		$this->VisitsModel->process($data2);
			

	}

}

?>