<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
date_default_timezone_set('Africa/Lagos');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Visit Me</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" rel="stylesheet"/>
   <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/mdb.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>
    <link rel="stylesheet" href="<?php echo base_url();?>css/visitortag.css"/>
    <link href="<?php echo base_url();?>sweetalert/sweetalert.css" rel="stylesheet">
            
    <script type="text/javascript">
        
// $(document).ready(function() {
// swal({ 
//   title: "Done",
//    text: "NDA Document hass been mailed successfully",
//     type: "success" 
//   },
//   function(){
//     if(screen.width <= 699) {
//         window.location.href = "welcome_message_mobile";
// // document.location = "welcome_message_mobile";
// }else{
//     // document.location = "welcome_message";
//     window.location.href = "welcome_message";
// }
// });
// });
    </script>
</head>
<body>
<?php
 foreach($data as $row):	
            
    $this->session->set_userdata('email_val', $row->emp_email);
    $this->session->set_userdata('employee_id', $row->employee_id);
        
        
         endforeach;

         
?>

<center>
<div id="container" class="tag">
<div class="col-md-12">
    <img style="width:100%; margin-top:10%;"  src="<?php echo base_url(); ?>img/thanks.png" />
    </div>


         <p>
            <?php 
            $createdat = $this->encrypt->decode($this->uri->segment(3));
            echo $createdat;
            ?>
         </p>



    <footer class="tag_footer">
        <p>Powered by <img style="width:100px;"  src="<?php echo base_url(); ?>img/company.png" /></p>
    </footer>

   
</div>
</center>
<script type='text/javascript' src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/mdb.min.js"></script>
    <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js'></script>
    <script  src="<?php echo base_url(); ?>js/index.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>sweetalert/sweetalert.min.js"></script>
</body>
</html>