<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Visitor Management Application</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" rel="stylesheet"/>
   <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/mdb.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/myStyle.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>

    
    <!-- Custom CSS  -->
    
    <!-- Custom MINIFIED CSS  -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.min.css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/cameraStyle.css" />
</head>
<body>
    <!-- <div class="info">
        <h1 class="yellow-text">Visit Me</h1>
        <span class="black-text">
            Made with <i class="fa fa-heart"></i> by
            <a href="http://www.google.com" class="black-text">Tech Barn</a><br />
        </span>
    </div> -->
    <!-- Modal -info -->
    <form class="steps" id="form_search" action="<?php echo site_url('checkin/search');?>">
        <!-- <center>
         
            <ul  id="progressbar">
                <li class="active">Basic Info</li>
                <li>Employee Info</li>
                <li>Camera </li>
                <li>NDA Info</li>
                
            </ul>
         
        </center> -->
        <!-- USER INFORMATION FIELD SET --> 
        <fieldset style="opacity: 0.9;">
            <h2 class="fs-title">Welcome to Techbarn limited:</h2>
            <h3 class="fs-subtitle">Kindlt fill out your details below to sign in</h3>
            
            <div class="row">
                <div class="col-md-6">
            <div class="md-form"style="margin-top:0 !important;" >
                <!-- <i class="fa fa-user prefix"></i> -->
                <input type="text" id="inputIconEx1" name="sender_firstname" class="form-control" />
                <label for="Fullname">Firstname</label>
            </div>
                </div>
                <div class="col-md-6">
                     <div class="md-form" style="margin-top:0;">
                <!-- <i class="fa fa-user prefix"></i> -->
                <input type="text" id="inputIconEx1" name="sender_lastname" class="form-control" />
                <label for="Fullname">Lastname</label>
            </div>
                </div>
            </div>
            
           
            
            <div class="md-form">
                <!-- <i class="fa fa-envelope prefix"></i> -->
                <input type="text" id="sender_email" name="sender_email" class="form-control" />
                <label for="Email Address">Email Address</label>
            </div>
            <div class="md-form">
                <!-- <i class="fa fa-phone prefix"></i> -->
                <input type="text" id="inputIconEx3" name="sender_phone" class="form-control" />
                <label for="Telephone">Telephone</label>
            </div>
          
            <div class="md-form">
                <!-- <i class="fa fa-group prefix"></i> -->
                <input type="text" id="inputIconEx4" name="sender_company" class="form-control" />
                <label for="Company">Company </label>
            </div>
           
            
          
            <!-- <button type="button" data-page="1" name="previous" class="btn btn-primary btn-rounded previous action-button" value="Previous">Previous</button> -->
            <button type="button" data-page="2" name="next" class=" hide btn btn-primary next action-button" value="Next">Next</button>
        </fieldset>
                <!-- Employee Info -->
        <fieldset>
        <p>Please input company (techbarn) employee name you would like to see.</p>
        
            <div class="md-form">
            
                <!-- <i class="fa fa-phone prefix"></i> -->
                <!-- <input type="text" id="inputIconEx4" class="form-control" /> -->
                
                <input type="text" name="title" class="form-control" id="title" value="<?php echo $row->emp_email;?>">
            </div>
           
            <div class="form-group">
              <div class="container">
                <div class="row">
                  <div class="col-md-5" style="padding-left: 0;" >
                <label for="exampleFormControlTextarea1">Purpose of Visit</label>
              </div>
            
                  <div>
                    <input type="radio" name="sender_radio" id="radio1" class="radio" value="Official" checked/>
                    <label for="radio1"name="sender_radio" value="Official">Official</label>
                    </div>&nbsp;&nbsp;
                    
                    <div>
                    <input type="radio" name="sender_radio" id="radio2" class="radio" value="Unofficial"/>
                    <label for="radio2"name="sender_radio" value="Official">Unofficial</label>
                    </div>
                    
              </div>
            </div>
            </div>
            <p>Comments:</p>
                <textarea class="form-control rounded-0" id="exampleFormControlTextarea1"placeholder="160 characters..." name="sender_purpose" rows="5"></textarea>
            </div>
        <button type="button" data-page="5" name="previous" class=" hide btn btn-primary previous action-button" value="Previous">Previous</button>

        <button type="button" data-page="3" name="next" class="hide btn btn-primary next action-button" value="Next">Next</button>

        </fieldset>
        <!-- Camera FIELD SET -->  
        <fieldset style="opacity: 0;">
            <h2 class="fs-title">Take a Picture</h2>
            <h3 class="fs-subtitle">Let us know how you look like :)?</h3>
            <div class="container">

                <div class="app">
              
                  <a href="#" id="start-camera" class="visible">Touch here to take a picture</a>
                  <video id="camera-stream"></video>
                  <img id="snap">
              
                  <p id="error-message"></p>
              
                  <div class="controls">
                    <a href="#" id="delete-photo" title="Delete Photo" class="disabled"><i class="fa fa-remove"></i></a>
                    <a href="#" id="take-photo" title="Take Photo"><i class="fa fa-camera"></i></a>
                    <a href="#" id="download-photo" download="selfie.png" title="Save Photo" class="disabled"><i class="fa fa-save"></i></a>  
                  </div>
              
                  <!-- Hidden canvas element. Used for taking snapshot of video. -->
                  <canvas></canvas>
              
                </div>
              
              </div>
            <br><br>
            <!-- End Final Calc -->
            <button type="button" data-page="5" name="previous" class=" hide btn btn-primary previous action-button" value="Previous">Previous</button>

        <button type="button" data-page="3" name="next" class=" hide btn btn-primary next action-button" value="Next">Next</button>
       
            <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">Need Help?</div>
        </fieldset>
      <!-- NDA -->
      <fieldset>
        <div class="md-form" style="margin-top:0;">
                   <h4 style="color:w">IHS NDA</h4>
                   <p>Welcome to IHS!</p>
                   <p style="text-align:justify"> <?php echo nl2br("We hope you enjoy your visit, While you're here, you may see or hear information that is confidential to IHS, its members or business partners.
                   You agree not to photograph or record any information that you come across during your visit. If you or your company has a nondisclosure agreement (NDA) with IHS, by clicking the check box below you acknowledge that information you see or hear during your visit is confidential under that NDA, and we agree that the terms of the NDA will prevail over this agreement.
                   
                   For those of you whose visit is not covered under an existing NDA, the information that you see or hear during your visit is the confidential information of IHS and you agree that you will not take,use, discuss, share or disclose this information without IHS's writeen permission. We agree that you don't need our permission if you can show that something is already generally publicly known through no fault of yours or breach by others.
                   
                   Thanks for your cooperation. Have a great day.
                   
                   ");?></p>
                   
                   
        </div>
         <button type="button" data-page="5" name="previous" class="hide btn btn-primary previous action-button" value="Previous">Previous</button>
            <button id="submit" class="  btn btn-primary large action-button next" type="submit" style="width:200px;" value="Submit">I Agree</button>
           
        </fieldset>
        
         
    </form>

    <script type='text/javascript' src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/mdb.min.js"></script>
    <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js'></script>
    <script  src="<?php echo base_url(); ?>js/index.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/camera.js"></script>

   <script src="<?php echo base_url();?>js/jquery-ui.js" type="text/javascript"></script>

<script type="text/javascript">
if(screen.width <= 699) {
document.location = "welcome_message_mobile";
// $('.hide').hide();
}
$(document).ready(function(){

		    $('#title').autocomplete({
                source: "<?php echo site_url('checkin/get_autocomplete');?>",
     
                // select: function (event, ui) {
                //     $(this).val(ui.item.label);
                //     $("#form_search").submit(); 
                // }
            });

		});
</script>

</body>
</html>