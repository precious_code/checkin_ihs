<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Visit Me</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" rel="stylesheet"/>
   <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/mdb.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/myStyle.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>

    
    <!-- Custom CSS  -->
    
    <!-- Custom MINIFIED CSS  -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.min.css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/cameraStyle.css" />
</head>
<body>
    <!-- <div class="info">
        <h1 class="yellow-text">Visit Me</h1>
        <span class="white-text">
            Made with <i class="fa fa-heart"></i> by
            <a href="http://www.google.com" class="white-text">Tech Barn</a><br />
        </span>
    </div> -->
    <!-- Modal -info -->
    <form class="steps" id="form_search" action="<?php echo site_url('receptionist/login');?>" method="post">
        
        <!-- USER INFORMATION FIELD SET --> 
        <fieldset style="opacity: 0.9;">
            <h2 class="fs-title">Receptionist Login</h2>
            <?php if(!empty($data)){echo $data;}?>
            <div class="md-form">
            
                <!-- <input type="text" id="inputIconEx4" class="form-control" /> -->
                
                <input type="email" name="user_email" class="form-control" id="user_email">
                <label for="user_email">Email</label>
            </div>
            <div class="md-form">
                <input type="password" id="user_password" name="user_password" class="form-control" />
                <label for="user_password">Password</label>
            </div>
            <button id="submit" class="btn btn-primary large action-button next" type="submit" value="Submit">Login</button>
            <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">Need Help?</div>
        </fieldset>
        <!-- <fieldset>
            <div class="explanation btn btn-small modal-trigger" data-modal-id="modal-3">Need Help222?</div>
        </fieldset> -->
    </form>

    <script type='text/javascript' src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/mdb.min.js"></script>
    <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js'></script>
    <script  src="<?php echo base_url(); ?>js/index.js"></script>

   <script src="<?php echo base_url();?>js/jquery-ui.js" type="text/javascript"></script>

<script type="text/javascript">

</script>

</body>
</html>