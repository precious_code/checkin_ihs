<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Visitor Management Application</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" rel="stylesheet"/>
   <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/mdb.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/myStyle.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>
    <link href="<?php echo base_url();?>sweetalert/sweetalert.css" rel="stylesheet">
    
    <!-- Custom CSS  -->
    
    <!-- Custom MINIFIED CSS  -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.min.css" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/cameraStyle.css" />
    <script>
    function myFn(){
        swal({ 
              title: "Done!!",
              text: "NDA Document has been mailed to you!",
              type: "success" 
        },
             function(){
                window.location.href=<?php echo site_url('welcome');?>;
        });
}
    </script>
</head>
<body>
    <!-- <div class="info">
        <h1 class="yellow-text">Visit Me</h1>
        <span class="black-text">
            Made with <i class="fa fa-heart"></i> by
            <a href="http://www.google.com" class="black-text">Tech Barn</a><br />
        </span>
    </div>
    -->
		<center>
    
        <img style="width:350px;padding-top:10%;"  src="<?php echo base_url(); ?>img/ihs-logo.png" /><br>
		<a class="btn btn-blue btn-hover btn-lg"style="width:200px;margin-top:10%;" href="<?php echo site_url('mindex');?>">SIGN IN</a>
    
    </cen
    </center>
    
    <script type='text/javascript' src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/mdb.min.js"></script>
    <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js'></script>
    <script  src="<?php echo base_url(); ?>js/index.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>js/camera.js"></script>

   <script src="<?php echo base_url();?>js/jquery-ui.js" type="text/javascript"></script>
   <script src="<?php echo base_url();?>sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">



$(document).ready(function(){

		    $('#title').autocomplete({
                source: "<?php echo site_url('mcheckin/get_autocomplete');?>",
     
                // select: function (event, ui) {
                //     $(this).val(ui.item.label);
                //     $("#form_search").submit(); 
                // }
            });

		});
</script>

</body>
</html>