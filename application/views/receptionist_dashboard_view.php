<!DOCTYPE html>
<html lang="en">
<?php	function nicetime($date)
	{
		if(empty($date)) {
			return "No date provided";
		}
		
		$periods         = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
		$lengths         = array("60","60","24","7","4.35","12","10");
		
		$now             = time();
		$unix_date       = strtotime($date);
		
		   // check validity of date
		if(empty($unix_date)) {    
			return "Bad date";
		}
	
		// is it future date or past date
		if($now > $unix_date) {    
			$difference     = $now - $unix_date;
			$tense         = "ago";
			
		} else {
			$difference     = $unix_date - $now;
			$tense         = "from now";
		}
		
		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
			$difference /= $lengths[$j];
		}
		
		$difference = round($difference);
		
		if($difference != 1) {
			$periods[$j].= "s";
		}
		
		return "$difference $periods[$j] {$tense}";
	}?>
<head>
    <title>Visit Me</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" rel="stylesheet"/>
   <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/mdb.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/myStyle.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery-ui.css"/>
    <!-- Style CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>css/new/style.css"/>

    
    <!-- Custom CSS  -->
    
    <!-- Custom MINIFIED CSS  -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.min.css" />
</head>

<body>
<center>
    <div class="" style="padding-bottom:40px;">
       
    </div>
</center>
        <!-- ***** Header Area Start ***** -->
        <header class="header-area sticky">
        <div class="container" >
            <div class="row">
                <div class="col-12">
                    <nav class="navbar navbar-expand-lg">
                        <!-- Logo -->
                        <a class="navbar-brand" style="color:white;" href="<?php echo site_url('Receptionist/dashboard'); ?>">Dashboard</a>
                        <!-- Navbar Toggler -->
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#worldNav" aria-controls="worldNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <!-- Navbar -->
                        <div class="collapse navbar-collapse" id="worldNav">
                            <ul class="navbar-nav ml-auto">
                               
                                <li class="nav-item">
                                    <a class="nav-link"style="color:white;"  href="<?php echo site_url('Receptionist/logout'); ?>"><i class="fa fa-sign-out"></i>Logout</a>
                                </li>
                               
                            </ul>
                            <!-- Search Form  -->
                            
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ***** Header Area End ***** -->
    <!--Main Navigation-->

    <!--Main layout-->
    <main class="container">
        <div class="container-fluid mt-5">


            <!--Grid row-->
            <div class="row wow fadeIn">

                <!--Grid column-->
                <div class="col-md-12 mb-4">

                    <!--Card-->
                    <div class="card">

                        <!--Card content-->
                        <div class="card-body">
                        <div class="content table-responsive table-full-width">
    <table class="table table-hover table-striped">
					<thead>
						<tr style="background-color:#CCCCCC;"class="test">
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Name</th>
                                        <th>Telephone</th>
                                        <th>Company</th>
                                        <th>Email</th>
                                        <th>Whom to See</th>
                                        <th>Reason</th>
                                        <th>Comment</th>
                                        <th>Print</th>
                        </tr>
						<thead>
                        <tr>
                        <?php 
                                    foreach ($data as $visit): ?>
                                    <tr>
                                        <th scope="row"><?php echo date('h:i A', strtotime($visit->createdat)) ?></th>
                                        <th><?php echo date('d-m-Y', strtotime($visit->createdat)); ?></th>
                                        <th><?php echo $visit->guestFullName; ?></th>
                                        <th><?php echo $visit->guestTelephone; ?></th>
                                        <th><?php echo $visit->guestCompany; ?></th>
                                        <th><?php echo $visit->guestEmail; ?></th>
                                        <th><?php echo $visit->employee; ?></th>
                                        <th><?php echo $visit->guestPurposeType; ?></th>
                                        <th><?php echo $visit->guestComment; ?></th>
                                        <th><a class="btn btn-blue" href="<?php echo base_url()?>/Receptionist/printtag/<?php echo $visit->createdat;?>">PRINT</a></th>
                                    </tr>
                                    
                                    <?php endforeach; ?>
                        </tr>
                        </table>
                        </div>
                            

                        </div>

                    </div>
                    <!--/.Card-->

                </div>
                <!--Grid column-->

              

            </div>
            <!--Grid row-->

        </div>
    </main>
    <!--Main layout-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
<script type='text/javascript' src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/mdb.min.js"></script>
    <!-- <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script> -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.js'></script>
    <script  src="<?php echo base_url(); ?>js/index.js"></script>
    <script src="<?php echo base_url();?>js/jquery-ui.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>sweetalert/sweetalert.min.js"></script>
    <!-- Initializations -->
    <script type="text/javascript">
        // Animations initialization
        new WOW().init();
    </script>

    <script>
         var time = new Date().getTime();
     $(document.body).bind("mousemove keypress", function(e) {
         time = new Date().getTime();
     });

     function refresh() {
         if(new Date().getTime() - time >= 60000) 
             window.location.reload(true);
         else 
             setTimeout(refresh, 10000);
     }

     setTimeout(refresh, 10000);
    </script>
</body>

</html>